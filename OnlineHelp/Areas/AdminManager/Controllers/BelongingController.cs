﻿using Model.Dao;
using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHelp.Areas.AdminManager.Controllers
{
    public class BelongingController : BaseController
    {
        // GET: AdminManager/Belonging
        public ActionResult Index(string searchString, int page = 1, int pageSize = 10)
        {
            var dao = new BelongingDao();
            var model = dao.ListAllPaging(searchString, page, pageSize);
            ViewBag.SearchString = searchString;
            ViewBag.RoomID = dao.ListRoom();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var dao = new BelongingDao();
            ViewBag.RoomID = dao.ListRoom();
            return View();
        }

        [HttpPost]
        public ActionResult Create(Belonging belonging)
        {
            if (ModelState.IsValid)
            {
                var dao = new BelongingDao();
                
                long id = dao.Insert(belonging);
                if (id > 0)
                {
                    return RedirectToAction("Index", "Belonging");
                }
                else
                {
                    ModelState.AddModelError("", "Thêm mới thành công");
                }
            }
            return View("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var dao = new BelongingDao();
            var model = dao.ViewDetail(id);
            ViewBag.RoomID = dao.ListRoom();
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Belonging belonging)
        {
            if (ModelState.IsValid)
            {
                var dao = new BelongingDao();
                ViewBag.RoomID = dao.ListRoom();
                var result = dao.Update(belonging);
                if (result)
                {
                    return RedirectToAction("Index", "Belonging");
                }
                else
                {
                    ModelState.AddModelError("", "Cập nhật phòng thành công");
                }
            }
            return View("Index");
        }

        public JsonResult Delete(int id)
        {
            var result = new BelongingDao().Delete(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }
    }
}