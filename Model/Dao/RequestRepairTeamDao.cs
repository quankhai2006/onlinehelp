﻿using Model.EF;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class RequestRepairTeamDao
    {
        OnlineHelpDbContext db = null;
        public RequestRepairTeamDao()
        {
            db = new OnlineHelpDbContext();
        }

        public IEnumerable<Request> ListAllPaging(string searchString, int page, int pageSize, int? type)
        {
            IQueryable<Request> model = db.Requests.Where(x=> x.Type == type);
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.Requestor.Contains(searchString) || x.Description.Contains(searchString));
            }
            return model.OrderByDescending(x => x.RequestID).ToPagedList(page, pageSize);
        }

        public List<Facility> ListFacility()
        {
            List<Facility> listFacility = db.Facilities.ToList();
            return listFacility;
        }

        public List<General> ListGeneral()
        {
            List<General> listGeneral = db.Generals.ToList();
            return listGeneral;
        }

        public List<Room> ListRoom()
        {
            List<Room> listRoom = db.Rooms.ToList();
            return listRoom;
        }
    }
}
