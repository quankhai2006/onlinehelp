﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHelp.Areas.AdminManager.Controllers
{
    public class HomeAdminController : BaseController
    {
        // GET: AdminManager/HomeAdmin
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetNotificationRequest()
        {
            var notificationRegisterTime = Session["LastUpdated"] != null ? Convert.ToDateTime(Session["LastUpdated"]) : DateTime.Now;
            NotificationComponent NC = new NotificationComponent();
            var list = NC.GetRequests(notificationRegisterTime);
            //update session here for get only new  requestDateTime
            Session["LastUpdate"] = DateTime.Now;
            return new JsonResult
            {
                Data = list,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }
}