﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineHelp.Areas.AdminManager.Models
{
    public class RequestChart
    {
        public string Type { get; set; }

        public int Amount { get; set; }


        public RequestChart(string Type,int Amount)
        {
            this.Type = Type;
            this.Amount = Amount;
        }
    }
}