﻿using Model.EF;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class RoomDao
    {
        OnlineHelpDbContext db = null;
        public RoomDao()
        {
            db = new OnlineHelpDbContext();
        }

        public long Insert(Room entity)
        {
            db.Rooms.Add(entity);
            db.SaveChanges();
            return entity.Id;
        }

        public List<Facility> ListAll()
        {
            List<Facility> listFacility = db.Facilities.ToList();
            return listFacility;
        } 
        public IEnumerable<Room> ListAllPaging(string searchString, int page, int pageSize)
        {
            IQueryable<Room> model = db.Rooms;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.Name.Contains(searchString));
            }
            return model.OrderByDescending(x => x.Id).ToPagedList(page, pageSize);
        }

        public Room ViewDetail(int id)
        {
            return db.Rooms.Find(id);
        }

        public bool Update(Room entity)
        {
            try
            {
                var room = db.Rooms.Find(entity.Id);
                room.Name = entity.Name;
                room.FacilityID = entity.FacilityID;
                db.SaveChanges();
                return true;
            }
            catch (Exception )
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var room = db.Rooms.Find(id);
                db.Rooms.Remove(room);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

    }
}
