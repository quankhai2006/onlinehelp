﻿var request = {
    init: function () {
        request.registerEvents();
    },
    registerEvents: function () {
        $('.btn-active').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/AdminManager/Request/ChangeStatus",
                data: { id: id },
                dataType: "json",
                type:"POST",
                success: function (response) {
                    console.log(response);
                    if (response.status == 0) {
                        btn.text('Undefined');
                        btn.css('background-color', '#867979');
                        
                    } else if (response.status == 1) {
                        btn.text('Fixing');
                        btn.css('background-color', 'red');
                        
                    } else {
                        btn.text('Finish');
                        btn.css('background-color', 'green');
                        
                    }
                }
            });
        });
    }
}
request.init();

var type = {
    init: function () {
        type.registerEvents();
    },
    registerEvents: function () {
        $('.btn-type').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/AdminManager/Request/ChangeType",
                data: { id: id },
                dataType: "json",
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response.type == 0) {
                        btn.text('Electrical repair');
                        btn.css('background-color', 'green');
                        
                    } else if (response.type == 1) {
                        btn.text('Water repair');
                        btn.css('background-color', 'green');
                        
                    } else if (response.type == 2) {
                        btn.text('Fix network');
                        btn.css('background-color', 'green');
                        
                    } else {
                        btn.text('Fixing utensils');
                        btn.css('background-color', 'green');
                        
                    }
                }
            });
        });
    }
}
type.init();

var level = {
    init: function () {
        level.registerEvents();
    },
    registerEvents: function () {
        $('.btn-level').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/AdminManager/Request/ChangeLevel",
                data: { id: id },
                dataType: "json",
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response.type == 0) {
                        btn.text('Undefined');
                        btn.css('background-color', '#867979');

                    } else if (response.type == 1) {
                        btn.text('Fix now');
                        btn.css('background-color', '#FFC300');

                    } else if (response.type == 2) {
                        btn.text('Serious');
                        btn.css('background-color', 'red');

                    } else if (response.type == 3) {
                        btn.text('Edit later');
                        btn.css('background-color', '#FF5733');
                    }
                }
            });
        });
    }
}
level.init();


$('.deleteDes').off('click').on('click', function (e) {
        e.preventDefault();
    var id = $(this).data('id');
    var con = confirm("Do you want to delete this request?");
        if (con) {
        $.ajax({
            url: "/AdminManager/Request/Delete/" + id,
            type: "POST",
            dataType: "json",
            success: function (res) {
                if (res.status == true) {
                    window.location.href = "/AdminManager/Request/Index";
                } else {
                    alert("Delete failed");
                }
            }
        });
    }
});

