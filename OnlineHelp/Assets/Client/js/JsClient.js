﻿window.onload = function () {
    document.getElementById("password1").onchange = validatePassword;
    document.getElementById("password2").onchange = validatePassword;
}

function validatePassword() {
    var pass2 = document.getElementById("password2").value;
    var pass1 = document.getElementById("password1").value;
    if (pass1 != pass2)
        document.getElementById("password2").setCustomValidity("Passwords Don't Match");
    else
        document.getElementById("password2").setCustomValidity('');
    //empty string means no validation error
}
    $(document).ready(function () {
        $("#btn-request").off('click').on('click', function (e) {
            var Requestor = $('#Requestor').val();
            var Email = $('#Email').val();
            var Description = $('#Description').val();
            var regex = /^([a-zA-Z0-9_.+-])+\@@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;


            if (Requestor == '') {
                $("#Requestor").val("");
                $("#Requestor").attr("placeholder", "Bạn chưa nhập tên.");
                $("#Requestor").addClass('placehoder');
            }





            if (Email == '') {
                $("#Email").val("");
                $("#Email").attr("placeholder", "Bạn chưa nhập email người nhận.");
                $("#Email").addClass('placehoder');
            } else if (regex.test(Email) == '') {
                $("#Email").val("");
                $("#Email").attr("placeholder", "Email không hợp lệ.");
                $("#Email").addClass('placehoder');
            }

            if (Description.length < 5) {
                $("#Description").val("");
                $("#Description").attr("placeholder", "Bạn chưa nhập nội dung cần gủi");
                $("#Description").addClass('placehoder');
            } else if (Description == '') {
                $("#Description").val("");
                $("#Description").attr("placeholder", "Bạn chưa nhập nội dung cần gủi");
                $("#Description").addClass('placehoder');
            }


            if (Requestor == '' || Email == '' || regex.test(Email) == ''
                || Description == '' || Description.length < 5
            ) {

            } else {
                e.preventDefault();
                $.ajax({
                    url: '/Home/Request',
                    data: {
                        Requestor: $("#Requestor").val(),
                        Email: $("#Email").val(),
                        Description: $("#Description").val(),
                        FacilityId: $("#comboboxFacility").val(),
                        RoomId: $("#comboboxRoom").val(),
                        BelongingId: $("#comboboxBelonging").val(),
                        Level: $("#comboboxLevel").val()
                    },
                    dataType: 'json',
                    type: 'POST',
                    success: function (res) {
                        if (res.status == true) {
                            swal("Thông báo", "Bạn đã gửi thông tin liên hệ thành công. Chúng tôi sẽ liên hệ sớm với bạn.", "success")
                                .then((value) => {
                                    window.location.href = "/Home/Index";
                                });
                        } else {
                            swal({
                                title: "Thông báo",
                                text: "Lỗi rồi. Liên hệ của bạn chưa được gửi. Vui lòng thử lại!",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            })
                        }
                    }
                });
            }
        });
    });

        $(document).ready(function () {
            $("#btn-contact").off('click').on('click', function (e) {
                var NameContact = $('#NameContact').val();
                var EmailContact = $('#EmailContact').val();
                var DescriptionContact = $('#DescriptionContact').val();
                var PhoneContact = $('#PhoneContact').val();
                var regex = /^([a-zA-Z0-9_.+-])+\@@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;


                if (NameContact == '') {
                    $("#NameContact").val("");
                    $("#NameContact").attr("placeholder", "Bạn chưa nhập tên.");
                    $("#NameContact").addClass('placehoder');
                }





                if (EmailContact == '') {
                    $("#EmailContact").val("");
                    $("#EmailContact").attr("placeholder", "Bạn chưa nhập email người nhận.");
                    $("#EmailContact").addClass('placehoder');
                } else if (regex.test(EmailContact) == '') {
                    $("#EmailContact").val("");
                    $("#EmailContact").attr("placeholder", "Email không hợp lệ.");
                    $("#EmailContact").addClass('placehoder');
                }

                if (DescriptionContact.length < 5) {
                    $("#DescriptionCongtact").val("");
                    $("#DescriptionContact").attr("placeholder", "Bạn chưa nhập nội dung cần gủi");
                    $("#DescriptionContact").addClass('placehoder');
                } else if (DescriptionContact == '') {
                    $("#DescriptionContact").val("");
                    $("#DescriptionContact").attr("placeholder", "Bạn chưa nhập nội dung cần gủi");
                    $("#DescriptionContact").addClass('placehoder');
                }

                if (PhoneContact == '') {
                    $("#PhoneContact").val("");
                    $("#PhoneContact").attr("placeholder", "Bạn chưa nhập tên.");
                    $("#PhoneContact").addClass('placehoder');
                }

                if (NameContact == '' || EmailContact == '' || PhoneContact == '' || regex.test(EmailContact) == ''
                    || DescriptionContact == '' || DescriptionContact.length < 5
                ) {

                } else {
                    e.preventDefault();
                    $.ajax({
                        url: '/Home/RequestContact',
                        data: {
                            Name: $("#NameContact").val(),
                            Email: $("#EmailContact").val(),
                            Phone: $("#PhoneContact").val(),
                            Description: $("#DescriptionContact").val(),
                        },
                        dataType: 'json',
                        type: 'POST',
                        success: function (res) {
                            if (res.status == true) {
                                swal("Thông báo", "Bạn đã gửi thông tin liên hệ thành công. Chúng tôi sẽ liên hệ sớm với bạn.", "success")
                                    .then((value) => {
                                        window.location.href = "/Home/Index";
                                    });
                            } else {
                                swal({
                                    title: "Thông báo",
                                    text: "Lỗi rồi. Liên hệ của bạn chưa được gửi. Vui lòng thử lại!",
                                    icon: "warning",
                                    buttons: true,
                                    dangerMode: true,
                                })
                            }
                        }
                    });
                }
            });
        });
    

        $(document).ready(function () {
            $('#comboboxFacility').on('change', function () {
                var facilityId = $('#comboboxFacility option:selected').val();
                $.ajax({
                    type: 'GET',
                    data: { facilityId: facilityId },
                    url: '@Url.Action("loadRoom","Home")',
                    success: function (result) {
                        var s = '<option value="-1">Select a Room</option>';
                        for (var i = 0; i < result.length; i++) {
                            s += '<option class="" value="' + result[i].Id + '">' + result[i].Name + '</option>'
                        }
                        $('#comboboxRoom').html(s);
                    }
                });
            });


        $('#comboboxRoom').on('change', function () {
            var roomId = $('#comboboxRoom option:selected').val();
            $.ajax({
            type: 'GET',
                data: {roomId: roomId },
        url: '@Url.Action("loadBelonging","Home")',
                success: function (result) {
                    var s = '<option value="-1">Select a Belongings</option>';
                    for (var i = 0; i < result.length; i++) {
            s += '<option value="' + result[i].Id + '">' + result[i].Name + '</option>'
        }
        $('#comboboxBelonging').html(s);
    }
});
});
});
$('.deleteDes').off('click').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var con = confirm("Bạn có muốn xóa yêu cầu này không");
    if (con) {
        $.ajax({
            url: "/Home/Delete/" + id,
            type: "POST",
            dataType: "json",
            success: function (res) {
                if (res.status == true) {
                    swal("Thông báo", "Xóa yêu cầu thành công.", "success")
                        .then((value) => {
                            window.location.href = "/";
                        })
                } else {
                    swal({
                        title: "Thông báo",
                        text: "Chưa xóa yêu cầu.",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true
                    }).then((value) => {
                        window.location.href = "/";
                    })
                }
            }
        })
    }
});
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}
$(document).ready(function () {
    /*
    var defaults = {
        containerID: 'toTop', // fading element id
        containerHoverID: 'toTopHover', // fading element hover id
        scrollSpeed: 1200,
        easingType: 'linear'
    };
    */
    $().UItoTop({
        easingType: 'easeOutQuart'
    });

});
jQuery(document).ready(function ($) {
    $(".scroll").click(function (event) {
        event.preventDefault();

        $('html,body').animate({
            scrollTop: $(this.hash).offset().top
        }, 1000);
    });
});
$(function () {
    //picturesEyes($('.demo li'));
    $('.demo li').picEyes();
});
$(function () {
    // Slideshow 4
    $("#slider3").responsiveSlides({
        auto: true,
        pager: true,
        nav: false,
        speed: 500,
        namespace: "callbacks",
        before: function () {
            $('.events').append("<li>before event fired.</li>");
        },
        after: function () {
            $('.events').append("<li>after event fired.</li>");
        }
    });

});