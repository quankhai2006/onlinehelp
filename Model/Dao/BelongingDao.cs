﻿using Model.EF;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class BelongingDao
    {
        OnlineHelpDbContext db = null;
        public BelongingDao()
        {
            db = new OnlineHelpDbContext();
        }

        public long Insert(Belonging entity)
        {
            db.Belongings.Add(entity);
            db.SaveChanges();
            return entity.Id;
        }

        public List<Room> ListRoom()
        {
            List<Room> list = db.Rooms.ToList();
            return list;
        }

        public IEnumerable<Belonging> ListAllPaging(string searchString, int page, int pageSize)
        {
            IQueryable<Belonging> model = db.Belongings;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.Name.Contains(searchString));
            }
            return model.OrderByDescending(x => x.Id).ToPagedList(page, pageSize);
        }

        public Belonging ViewDetail(int id)
        {
            return db.Belongings.Find(id);
        }

        public bool Update(Belonging entity)
        {
            try
            {
                var belonging = db.Belongings.Find(entity.Id);
                belonging.Name = entity.Name;
                belonging.RoomID = entity.RoomID;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var belonging = db.Belongings.Find(id);
                db.Belongings.Remove(belonging);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
