﻿using Model.Dao;
using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHelp.Areas.AdminManager.Controllers
{
    public class RequestController : BaseController
    {
        // GET: AdminManager/Request
        public ActionResult Index(string searchString, int page = 1, int pageSize = 10)
        {
            var dao = new RequestDao();
            var model = dao.ListAllPaging(searchString, page, pageSize);
            ViewBag.SearchString = searchString;
            ViewBag.ListFacility = dao.ListFacility();
            ViewBag.ListGeneral = dao.ListGeneral();
            ViewBag.ListRoom = dao.ListRoom();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Request request)
        {
            if (ModelState.IsValid)
            {
                var dao = new RequestDao();
                long id = dao.Insert(request);
                if (id > 0)
                {
                    return RedirectToAction("Index", "Request");
                }
                else
                {
                    ModelState.AddModelError("", "Thêm mới thành công");
                }
            }
            return View("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var request = new RequestDao().ViewDetail(id);
            return View(request);
        }

        [HttpPost]
        public ActionResult Edit(Request request)
        {
            if (ModelState.IsValid)
            {
                var dao = new RequestDao();
                var result = dao.Update(request);
                if (result)
                {
                    return RedirectToAction("Index", "Request");
                }
                else
                {
                    ModelState.AddModelError("", "Cập nhật người dùng thành công");
                }
            }
            return View("Index");
        }

        
        public JsonResult Delete(int id)
        {
            var result = new RequestDao().Delete(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }

        [HttpPost]
        public JsonResult ChangeStatus(int id)
        {
            var result = new RequestDao().ChangeStatus(id);
            return Json(new
            {
                status = result
            });
        }

        [HttpPost]
        public JsonResult ChangeType(int id)
        {
            var result = new RequestDao().ChangeType(id);
            return Json(new
            {
                type = result
            });
        }

        [HttpPost]
        public JsonResult ChangeLevel(int id)
        {
            var result = new RequestDao().ChangeLevel(id);
            return Json(new
            {
                type = result
            });
        }
    }
}