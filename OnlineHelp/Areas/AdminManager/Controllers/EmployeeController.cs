﻿using Model.Dao;
using Model.EF;
using OnlineHelp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHelp.Areas.AdminManager.Controllers
{
    public class EmployeeController : BaseController
    {
        // GET: AdminManager/Employee
        public ActionResult Index(string searchString, int page = 1, int pageSize = 10)
        {
            var dao = new EmployeeDao();
            var model = dao.ListAllPaging(searchString, page, pageSize);
            ViewBag.searchString = searchString;
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            if (ModelState.IsValid)
            {
                var dao = new EmployeeDao();
                //if (!string.IsNullOrEmpty(user.Password))
                //{
                var encryptorMd5Pas = Encryptor.MD5Hash(employee.Password);
                employee.Password = encryptorMd5Pas;
                //}

                long id = dao.Insert(employee);
                if (id > 0)
                {
                    return RedirectToAction("Index", "Employee");
                }
                else
                {
                    ModelState.AddModelError("", "Thêm Employee thành công");
                }
            }
            return View("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var employee = new EmployeeDao().ViewDetail(id);
            return View(employee);
        }

        [HttpPost]
        public ActionResult Edit(Employee employee)
        {
            if (ModelState.IsValid)
            {
                var dao = new EmployeeDao();
                if (!string.IsNullOrEmpty(employee.Password))
                {
                    var encryptedMd5Pas = Encryptor.MD5Hash(employee.Password);
                    employee.Password = encryptedMd5Pas;
                }
                var result = dao.Update(employee);
                if (result)
                {
                    return RedirectToAction("Index", "Employee");
                }
                else
                {
                    ModelState.AddModelError("", "Update success");
                }
            }
            return View("Index");
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            new EmployeeDao().Delete(id);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult ChangeStatus(long id)
        {
            var result = new EmployeeDao().ChangeStatus(id);
            return Json(new
            {
                status = result
            });
        }

        [HttpPost]
        public JsonResult ChangeType(int id)
        {
            var result = new EmployeeDao().ChangeType(id);
            return Json(new
            {
                type = result
            });
        }
    }
}