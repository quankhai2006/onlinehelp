﻿using Model.Dao;
using OnlineHelp.Areas.AdminManager.Models;
using OnlineHelp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHelp.Areas.RepairTeam.Controllers
{
    public class LoginRepairTeamController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var dao = new EmployeeDao();
                var result = dao.Login(model.UserName, Encryptor.MD5Hash(model.Password));
                if (result == 1)
                {
                    var user = dao.GetById(model.UserName);
                    var employeeSession = new EmployeeLogin();
                    employeeSession.UserName = user.UserName;
                    employeeSession.EmployeeID = user.ID;
                    employeeSession.Avatar = user.Avartar;
                    employeeSession.Name = user.Name;
                    employeeSession.Type = user.Type;
                    Session.Add(CommonConstants.EMPLOYEE_SESSION, employeeSession);
                    return RedirectToAction("Index", "HomeRepairTeam");
                }
                else if (result == 0)
                {
                    ModelState.AddModelError("", "Tài khoản không tồn tại.");
                }
                else if (result == -1)
                {
                    ModelState.AddModelError("", "Tài khoản đang bị khóa.");
                }
                else if (result == -2)
                {
                    ModelState.AddModelError("", "Mật khẩu không đúng.");
                }
                else
                {
                    ModelState.AddModelError("", "Đăng nhập không đúng.");
                }
            }
            return View("Index");
        }
    }
}
