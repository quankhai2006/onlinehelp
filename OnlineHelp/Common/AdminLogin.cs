﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineHelp.Common
{
    [Serializable]
    public class AdminLogin
    {
        public long AdminID { get; set; }
        public string UserName { get; set; }
        public string Avatar { get; set; }
        public string Name { get; set; }
    }
}