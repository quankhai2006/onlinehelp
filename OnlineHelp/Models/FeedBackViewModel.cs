﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineHelp.Models
{
    public class FeedBackViewModel
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public int Phone { get; set; }

        public string Description { get; set; }

        public DateTime CreatedAt { get; set; }

        public int UserId { get; set; }
    }
}