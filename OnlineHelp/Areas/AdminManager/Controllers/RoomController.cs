﻿using Model.Dao;
using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHelp.Areas.AdminManager.Controllers
{
    public class RoomController : BaseController
    {
        // GET: AdminManager/Room
        public ActionResult Index(string searchString, int page = 1, int pageSize = 10)
        {
            var dao = new RoomDao();
            var model = dao.ListAllPaging(searchString, page, pageSize);
            ViewBag.SearchString = searchString;
            ViewBag.FacilityID = dao.ListAll();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var dao = new RoomDao();
            ViewBag.FacilityID = dao.ListAll();
            return View();
        }

        [HttpPost]
        public ActionResult Create(Room room)
        {
            if (ModelState.IsValid)
            {
                var dao = new RoomDao();
                ViewBag.FacilityID = dao.ListAll();
                long id = dao.Insert(room);
                if (id > 0)
                {
                    return RedirectToAction("Index", "Room");
                }
                else
                {
                    ModelState.AddModelError("", "Thêm mới thành công");
                }
            }
            return View("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var room = new RoomDao();
            var model = room.ViewDetail(id);
            ViewBag.FacilityID = room.ListAll();
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Room room)
        {
            if (ModelState.IsValid)
            {
                var dao = new RoomDao();
                ViewBag.FacilityID = dao.ListAll();
                var result = dao.Update(room);
                if (result)
                {
                    return RedirectToAction("Index", "Room");
                }
                else
                {
                    ModelState.AddModelError("", "Cập nhật phòng thành công");
                }
            }
            return View("Index");
        }

        public JsonResult Delete(int id)
        {
            var result = new RoomDao().Delete(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }
    }
}