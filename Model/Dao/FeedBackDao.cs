﻿using Model.EF;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class FeedBackDao
    {
        OnlineHelpDbContext db = null;
        public FeedBackDao()
        {
            db = new OnlineHelpDbContext();
        }

        public long Insert(FeedBack entity)
        {
            db.FeedBacks.Add(entity);
            db.SaveChanges();
            return entity.Id;
        }

        public IEnumerable<FeedBack> ListAll(int page, int pageSize)
        {
            IQueryable<FeedBack> model = db.FeedBacks.Where(x=>x.Status == 1);
            return model.OrderByDescending(x => x.CreatedAt).ToPagedList(page, pageSize);
        }

        public IEnumerable<FeedBack> ListAllPaging(string searchString, int page, int pageSize)
        {
            IQueryable<FeedBack> model = db.FeedBacks;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.Name.Contains(searchString) || x.Description.Contains(searchString));
            }
            return model.OrderByDescending(x => x.Name).ToPagedList(page, pageSize);
        }

        public int ChangeStatus(long id)
        {
            var feedBack = db.FeedBacks.Find(id);
            if (feedBack.Status == 0)
            {
                feedBack.Status = 1;
                feedBack.CreatedAt = DateTime.Now;
            }
            else if (feedBack.Status == 1)
            {
                feedBack.Status = 0;
            }
            db.SaveChanges();
            return Convert.ToInt32(feedBack.Status);
        }
    }
}
