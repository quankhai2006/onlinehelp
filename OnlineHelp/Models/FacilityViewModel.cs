﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineHelp.Models
{
    public class FacilityViewModel
    {
        public string Requestor { get; set; }

        public int FacilityId { get; set; }

        public int RoomId { get; set; }

        public int BelongingId { get; set; }

        public string Email { get; set; }

        public string Description { get; set; }

        public int UserId { get; set; }

        public int Status { get; set; }

        public int Type { get; set; }

        public int Level { get; set; }

        public DateTime? RequestDateTime { get; set; }
    }
}