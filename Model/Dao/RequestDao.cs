﻿using Model.EF;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class RequestDao
    {
        OnlineHelpDbContext db = null;
        public RequestDao()
        {
            db = new OnlineHelpDbContext();
        }

        public long Insert(Request entity)
        {
            db.Requests.Add(entity);
            db.SaveChanges();
            return entity.RequestID;
        }

        public long InsertGeneral(General entity)
        {
            db.Generals.Add(entity);
            db.SaveChanges();
            return entity.Id;
        }

        public List<Facility> ListAll()
        {
            return db.Facilities.ToList();
        }

        public List<Request> ListLevel()
        {
            return db.Requests.ToList();
        }

        public IEnumerable<Request> ListNotification(int page, int pageSize)
        {
            IQueryable<Request> model = db.Requests.Where(x => x.Status == 2);
            return model.OrderByDescending(x => x.UpdatedAt).ToPagedList(page, pageSize);
        }

        public IEnumerable<Request> ListPostRequest(int page, int pageSize)
        {
            IQueryable<Request> model = db.Requests;
            return model.OrderByDescending(x => x.RequestDateTime).ToPagedList(page, pageSize);
        }

        public IEnumerable<Request> ListAllPaging(string searchString, int page, int pageSize)
        {
            IQueryable<Request> model = db.Requests;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.Requestor.Contains(searchString) || x.Description.Contains(searchString));
            }
            return model.OrderByDescending(x => x.RequestID).ToPagedList(page, pageSize);
        }

        public bool Update(Request entity)
        {
            try
            {
                var request = db.Requests.Find(entity.RequestID);
                request.Requestor = entity.Requestor;
                request.GeneralID = entity.GeneralID;
                request.Description = entity.Description;
                
                
                db.SaveChanges();
                return true;
            }
            catch (Exception )
            {
                return false;
            }
        }

        public Request GetById(string requestor)
        {
            return db.Requests.SingleOrDefault(x => x.Requestor == requestor);
        }

        public Request ViewDetail(int id)
        {
            return db.Requests.Find(id);
        }

        public bool Delete(int id)
        {
            try
            {
                var request = db.Requests.Find(id);
                db.Requests.Remove(request);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public int ChangeStatus(long id)
        {
            var request = db.Requests.Find(id);
            if (request.Status == 0)
            {
                request.Status = 1;
            }
            else if (request.Status == 1)
            {
                request.Status = 2;
                request.UpdatedAt = DateTime.Now;
            }
            else if (request.Status == 2)
            {
                request.Status = 0;
            }
            db.SaveChanges();
            return Convert.ToInt32(request.Status);
        }

        public int ChangeType(long id)
        {
            var request = db.Requests.Find(id);
            if (request.Type == 0)
            {
                request.Type = 1;
            }
            else if (request.Type == 1)
            {
                request.Type = 2;
            }
            else if (request.Type == 2)
            {
                request.Type = 3;
            }
            else if (request.Type == 3)
            {
                request.Type = 0;
            }
            db.SaveChanges();
            return Convert.ToInt32(request.Type);
        }

        public int ChangeLevel(long id)
        {
            var request = db.Requests.Find(id);
            if (request.Level == 0)
            {
                request.Level = 1;
            }
            else if (request.Level == 1)
            {
                request.Level = 2;
            }
            else if (request.Level == 2)
            {
                request.Level = 3;
            }
            else if (request.Level == 3)
            {
                request.Level = 0;
            }
            db.SaveChanges();
            return Convert.ToInt32(request.Level);
        }

        public int ListCountRequest()
        {
            var listCount = db.Requests.Count();
            return listCount;
        }

        public int ListCountRequestFinish()
        {
            var listCount = db.Requests.Where(x => x.Status == 2).Count();
            return listCount;
        }

        public int ListCountRequestNotFinish()
        {
            var listCount = db.Requests.Where(x => x.Status == 1).Count();
            return listCount;
        }

        public  List<Facility> ListFacility()
        {
            List<Facility> listFacility = db.Facilities.ToList();
            return listFacility;
        }

        public List<General> ListGeneral()
        {
            List<General> listGeneral = db.Generals.ToList();
            return listGeneral;
        }

        public List<Room> ListRoom()
        {
            List<Room> listRoom = db.Rooms.ToList();
            return listRoom;
        }

        public List<User> ListUser()
        {
            List<User> listUser = db.Users.ToList();
            return listUser;
        }

        public int ListTypeRequestChart(int type)
        {
            var listChart = db.Requests.Where(x => x.Type == type).Count();
            return listChart;
        }
    }
}
