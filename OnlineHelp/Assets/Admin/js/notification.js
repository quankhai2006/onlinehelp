﻿$(function () {
    //Click on Notification icon for show notification
    $('#noti').click(function (e) {
        e.stopPropagation();
        $('.noti-content').show();
        var count = 0;
        count = parseInt($('span.count').html()) || 0;
        // only load notitfication if not already loaded
        if (count > 0) {
            updateNotification();
        }
        $('span.count', this).html('&nbsp;');
    })
    //hide notification
    $('html').click(function () {
        $('.noti-content').hide();
    })
    //update notification
    function updateNotification() {
        $('#notiContent').empty();
        $('#notiContent').append($('<li>Loading....</li>'));

        $.ajax({
            type: 'GET',
            url: '/AdminManager/HomeAdmin/GetNotificationRequest',
            success: function (response) {
                $('#notiContent').empty();
                if (response.length == 0) {
                    $('#notiContent').append($('<li>No data available</li>'));

                }
                $.each(response, function (index, value) {
                    $('#notiContent').append($('<li><a href="/AdminManager/Request/Index/">New request : ' + value.Requestor + ' với nội dung : ' + value.Description + '</a></li>'));

                });
            },
            error: function (error) {
                console.log(error);
            }
        })
    }
    //update notification acount
    function updateNotificationCount() {
        var count = 0;
        count = parseInt($('span.count').html()) || 0;
        count++;
        $('span.count').html(count);
    }
    //signalr js code for start hub and send receive notification
    var notificationHub = $.connection.notificationHub;
    $.connection.hub.start().done(function () {
        console.log('Notification hub started');
    });
    //signalr method for push server message to client
    notificationHub.client.notify = function (message) {
        if (message && message.toLowerCase() == "added") {
            updateNotificationCount();
        }
    }
})