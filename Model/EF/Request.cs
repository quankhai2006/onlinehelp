namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Request")]
    public partial class Request
    {
        public long RequestID { get; set; }

        [StringLength(50)]
        public string Requestor { get; set; }

        public long? GeneralID { get; set; }

        public DateTime? RequestDateTime { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public int? Status { get; set; }

        public int? Type { get; set; }

        public int? Level { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        public long? UserID { get; set; }

        public DateTime? UpdatedAt { get; set; }
    }
}
