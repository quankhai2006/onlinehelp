﻿using Model.Dao;
using Model.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHelp.Areas.AdminManager.Controllers
{
    public class FacilityController : BaseController
    {
        // GET: AdminManager/Facility
        public ActionResult Index(string searchString, int page = 1, int pageSize = 10)
        {
            var dao = new FacilityDao();
            var model = dao.ListAllPaging(searchString, page, pageSize);
            ViewBag.SearchString = searchString;
            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Facility facility)
        {
            if (ModelState.IsValid)
            {
                var dao = new FacilityDao();
                long id = dao.Insert(facility);
                if (id > 0)
                {
                    return RedirectToAction("Index", "Facility");
                }
                else
                {
                    ModelState.AddModelError("", "Thêm mới thành công");
                }
            }
            return View("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var facility = new FacilityDao().ViewDetail(id);
            return View(facility);
        }

        [HttpPost]
        public ActionResult Edit(Facility facility)
        {
            if (ModelState.IsValid)
            {
                var dao = new FacilityDao();
                var result = dao.Update(facility);
                if (result)
                {
                    return RedirectToAction("Index", "Facility");
                }
                else
                {
                    ModelState.AddModelError("", "Cập nhật người dùng thành công");
                }
            }
            return View("Index");
        }

        public JsonResult Delete(int id)
        {
            var result = new FacilityDao().Delete(id);
            if (result == true)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }
    }
}