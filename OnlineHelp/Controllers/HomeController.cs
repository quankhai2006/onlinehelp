﻿using Common;
using Model.Dao;
using Model.EF;
using OnlineHelp.Common;
using OnlineHelp.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHelp.Controllers
{
    public class HomeController : Controller
    {
        OnlineHelpDbContext db = new OnlineHelpDbContext();
        public ActionResult Index(int page = 1, int pageSize = 5)
        {
            var dao = new RequestDao();
            var dao1 = new FacilityDao();
            var dao2 = new FeedBackDao();
            ViewBag.notification = dao.ListNotification(page, pageSize);
            ViewBag.facility = dao.ListAll();
            ViewBag.requestIdCount = dao.ListCountRequest();
            ViewBag.requestIdCountFinish = dao.ListCountRequestFinish();
            ViewBag.requestIdCountNotFinish = dao.ListCountRequestNotFinish();
            ViewBag.ListRequest = dao.ListPostRequest(page, pageSize = 3);
            ViewBag.ListGeneral = dao.ListGeneral();
            ViewBag.ListRoom = dao.ListRoom();
            ViewBag.ListUser = dao.ListUser();
            ViewBag.ListFeedBack = dao2.ListAll(page,pageSize = 1);
            ViewBag.ListCountFacility = dao1.ListCountFacility();
            var userID = (UserLogin)Session[CommonConstants.USER_SESSION];
            if (userID != null)
            {
                List<Request> result1 = db.Requests.Where(x => x.UserID == userID.UserID).ToList();
                Session["ListDescription"] = result1;
            }
            return View();
        }

        [HttpPost]
        public JsonResult Request(FacilityViewModel model)
        {
            var dao = new RequestDao();

            General ad = new General()
            {
                FacilityId = model.FacilityId,
                RoomId = model.RoomId,
                BelongingsId = model.BelongingId
            };

            long id = dao.InsertGeneral(ad);
            var userSession = (UserLogin)Session[OnlineHelp.Common.CommonConstants.USER_SESSION];
            Request r = new Request()
            {
                Requestor = model.Requestor,
                Email = model.Email,
                Description = model.Description,
                UserID = userSession.UserID,
                Status = model.Status,
                Type = model.Type,
                Level = model.Level,
                RequestDateTime = DateTime.Now,
                GeneralID = id
            };

            long id1 = dao.Insert(r);


            if (id > 0 && id1 > 0)
            {
                string content = System.IO.File.ReadAllText(Server.MapPath("~/Assets/Client/template/newrequest.html"));

                content = content.Replace("{{Requestor}}", model.Requestor);
                content = content.Replace("{{Email}}", model.Email);
                content = content.Replace("{{Description}}", model.Description);
                var toEmail = ConfigurationManager.AppSettings["ToEmailAddress"].ToString();

                new MailHelper().SendMail(model.Email, "Yêu cầu gửi từ OnlineHelp", content);
                new MailHelper().SendMail(toEmail, "Yêu cầu gửi từ OnlineHelp", content);
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }

        public ActionResult loadRoom(int facilityId)
        {
            return Json(db.Rooms.Where(s => s.FacilityID == facilityId).Select(s => new {
                Id = s.Id,
                Name = s.Name,
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult loadBelonging(int roomId)
        {
            return Json(db.Belongings.Where(c => c.RoomID == roomId).Select(c => new {
                Id = c.Id,
                Name = c.Name,
            }).ToList(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Login(LoginModel model)
        {
            var dao1 = new RequestDao();
            int page = 1, pageSize = 5;
            var dao2 = new FeedBackDao();
            if (ModelState.IsValid)
            {
                var dao = new UserDao();
                var result = dao.Login(model.UserName, Encryptor.MD5Hash(model.Password));
                if (result == 1)
                {
                    var user = dao.GetById(model.UserName);
                    var userSession = new UserLogin();
                    userSession.UserName = user.UserName;
                    userSession.UserID = user.ID;
                    userSession.Name = user.Name;
                    Session.Add(CommonConstants.USER_SESSION, userSession);
                    Session["checkLogin"] = null;
                    return Redirect("/");
                }
                else
                {
                    ViewBag.facility = dao1.ListAll();
                    ViewBag.notification = dao1.ListNotification(page, pageSize);
                    ViewBag.requestIdCount = dao1.ListCountRequest();
                    ViewBag.requestIdCountFinish = dao1.ListCountRequestFinish();
                    ViewBag.requestIdCountNotFinish = dao1.ListCountRequestNotFinish();
                    ViewBag.ListRequest = dao1.ListPostRequest(page, pageSize = 3);
                    ViewBag.ListGeneral = dao1.ListGeneral();
                    ViewBag.ListRoom = dao1.ListRoom();
                    ViewBag.ListUser = dao1.ListUser();
                    ViewBag.ListFeedBack = dao2.ListAll(page, pageSize = 1);
                    Session["checkLogin"] = 1;
                    ViewBag.LoginError = "Tài khoản hoặc mật khẩu không tồn tại";
                    return View("Index");
                }
            }
            ViewBag.LoginError = "Tài khoản hoặc mật khẩu không tồn tại";
            ViewBag.facility = dao1.ListAll();
            ViewBag.notification = dao1.ListNotification(page, pageSize);
            ViewBag.requestIdCount = dao1.ListCountRequest();
            ViewBag.requestIdCountFinish = dao1.ListCountRequestFinish();
            ViewBag.requestIdCountNotFinish = dao1.ListCountRequestNotFinish();
            ViewBag.ListRequest = dao1.ListPostRequest(page, pageSize = 3);
            ViewBag.ListGeneral = dao1.ListGeneral();
            ViewBag.ListRoom = dao1.ListRoom();
            ViewBag.ListUser = dao1.ListUser();
            ViewBag.ListFeedBack = dao2.ListAll(page, pageSize = 1);
            Session["checkLogin"] = 1;
            return View("Index");
        }
        public ActionResult Logout()
        {
            Session[CommonConstants.USER_SESSION] = null;
            return Redirect("/");
        }
        [HttpPost]
        public ActionResult Setting(SettingModel settingModel)
        {
            var dao2 = new RequestDao();
            var dao3 = new FeedBackDao();
            if (ModelState.IsValid)
            {
                var dao = new UserDao();
                int page = 1, pageSize = 5;
                var result1 = dao.Setting(settingModel.Name, settingModel.UserName, Encryptor.MD5Hash(settingModel.Password), Encryptor.MD5Hash(settingModel.NewPassword));
                if (result1 == true)
                {
                    return Redirect("/");
                }
                else
                {
                    ViewBag.facility = dao2.ListAll();
                    ViewBag.notification = dao2.ListNotification(page, pageSize);
                    ViewBag.requestIdCount = dao2.ListCountRequest();
                    ViewBag.requestIdCountFinish = dao2.ListCountRequestFinish();
                    ViewBag.requestIdCountNotFinish = dao2.ListCountRequestNotFinish();
                    ViewBag.ListRequest = dao2.ListPostRequest(page, pageSize = 3);
                    ViewBag.ListGeneral = dao2.ListGeneral();
                    ViewBag.ListRoom = dao2.ListRoom();
                    ViewBag.ListUser = dao2.ListUser();
                    ViewBag.ListFeedBack = dao3.ListAll(page, pageSize = 1);
                    return View("Index");
                }
            }
            ViewBag.facility = dao2.ListAll();
            return View("Index");
        }
        public JsonResult Delete(int id)
        {
            RequestDao daor = new RequestDao();
            var result = daor.Delete(id);
            return Json(new
            {
                status = true
            });
        }

        [HttpPost]
        public JsonResult RequestContact(FeedBackViewModel model)
        {
            var dao = new FeedBackDao();
            var userSession = (UserLogin)Session[OnlineHelp.Common.CommonConstants.USER_SESSION];
            FeedBack ad = new FeedBack()
            {
                Name = model.Name,
                Email = model.Email,
                Phone = model.Phone,
                Description = model.Description,
                UserID = userSession.UserID,
                CreatedAt = DateTime.Now
                
            };
            long id = dao.Insert(ad);
            if (id > 0)
            {
                return Json(new
                {
                    status = true
                });
            }
            else
            {
                return Json(new
                {
                    status = false
                });
            }
        }
    }
}