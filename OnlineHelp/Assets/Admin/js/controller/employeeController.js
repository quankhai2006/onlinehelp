﻿var employee = {
    init: function () {
        employee.registerEvents();
    },
    registerEvents: function () {
        $('.btn-active').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/AdminManager/Employee/ChangeStatus",
                data: { id: id },
                dataType: "json",
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response.status == true) {
                        btn.text('Active');
                        btn.css('background-color', 'green');
                    }
                    else {
                        btn.text('Lock');
                        btn.css('background-color', 'red');
                    }
                }
            });
        });
    }
}
employee.init();

var type = {
    init: function () {
        type.registerEvents();
    },
    registerEvents: function () {
        $('.btn-type').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/AdminManager/Employee/ChangeType",
                data: { id: id },
                dataType: "json",
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response.type == 0) {
                        btn.text('Electrical repair');
                        btn.css('background-color', 'green');

                    } else if (response.type == 1) {
                        btn.text('Water repair');
                        btn.css('background-color', 'green');

                    } else if (response.type == 2) {
                        btn.text('Fix network');
                        btn.css('background-color', 'green');

                    } else {
                        btn.text('Fixing utensils');
                        btn.css('background-color', 'green');

                    }
                }
            });
        });
    }
}
type.init();