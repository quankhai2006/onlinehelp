﻿var admin = {
    init: function () {
        admin.registerEvents();
    },
    registerEvents: function () {
        $('.btn-active').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/AdminManager/Admin/ChangeStatus",
                data: { id: id },
                dataType: "json",
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response.status == true) {
                        btn.text('Active');
                        btn.css('background-color', 'green');
                    }
                    else {
                        btn.text('Lock');
                        btn.css('background-color', 'red');
                    }
                }
            });
        });
    }
}
admin.init();