﻿using Model.Dao;
using Model.EF;
using OnlineHelp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHelp.Areas.AdminManager.Controllers
{
    public class AdminController : BaseController
    {
        // GET: AdminManager/Admin
        public ActionResult Index(string searchString, int page = 1, int pageSize = 5)
        {
            var dao = new AdminDao();
            var model = dao.ListAllPaging(searchString, page, pageSize);
            ViewBag.searchString = searchString;
            return View(model);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Admin admin)
        {
            if (ModelState.IsValid)
            {
                var dao = new AdminDao();
                var encryptorMd5Pas = Encryptor.MD5Hash(admin.Password);
                admin.Password = encryptorMd5Pas;
                long id = dao.Insert(admin);
                if (id > 0)
                {
                    return RedirectToAction("Index", "Admin");
                }
                else
                {
                    ModelState.AddModelError("", "Thêm Admin không thành công!");
                }
            }
            return View("Index");
        }

        public ActionResult Edit(int id)
        {
            var admin = new AdminDao().ViewDetail(id);
            return View(admin);
        }
        [HttpPost]
        public ActionResult Edit(Admin admin)
        {
            if (ModelState.IsValid)
            {
                var dao = new AdminDao();
                if (!string.IsNullOrEmpty(admin.Password))
                {
                    var encryptedMd5Pas = Encryptor.MD5Hash(admin.Password);
                    admin.Password = encryptedMd5Pas;
                }
                var result = dao.Update(admin);
                if (result)
                {
                    return RedirectToAction("Index", "Admin");
                }
                else
                {
                    ModelState.AddModelError("", "Cập nhật Admin không thành công.");
                }
            }
            return View("Index");
        }
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            new AdminDao().Delete(id);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public JsonResult ChangeStatus(long id)
        {
            var result = new AdminDao().ChangeStatus(id);
            return Json(new
            {
                status = result
            });
        }
    }
}