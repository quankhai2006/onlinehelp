﻿using OnlineHelp.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace OnlineHelp.Areas.RepairTeam.Controllers
{
    public class BaseRepairTeamController : Controller
    {
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var session = (EmployeeLogin)Session[CommonConstants.EMPLOYEE_SESSION];
            if (session == null)
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "LoginRepairTeam", action = "Index", Area = "RepairTeam" }));
            }
            base.OnActionExecuted(filterContext);
        }
    }
}