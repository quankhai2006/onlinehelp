﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHelp.Areas.RepairTeam.Controllers
{
    public class HomeRepairTeamController : BaseRepairTeamController
    {
        // GET: RepairTeam/HomeRepairTeam
        public ActionResult Index()
        {
            return View();
        }
    }
}