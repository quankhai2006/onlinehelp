﻿using System.Web.Mvc;

namespace OnlineHelp.Areas.RepairTeam
{
    public class RepairTeamAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "RepairTeam";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "RepairTeam_default",
                "RepairTeam/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}