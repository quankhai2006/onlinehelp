﻿using Model.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHelp.Areas.AdminManager.Controllers
{
    public class FeedBackController : BaseController
    {
        // GET: AdminManager/FeedBack
        public ActionResult Index(string searchString, int page = 1, int pageSize = 10)
        {
            var dao = new FeedBackDao();
            var model = dao.ListAllPaging(searchString, page, pageSize);
            ViewBag.SearchString = searchString;
            
            return View(model);
        }

        [HttpPost]
        public JsonResult ChangeStatus(int id)
        {
            var result = new FeedBackDao().ChangeStatus(id);
            return Json(new
            {
                status = result
            });
        }
    }
}