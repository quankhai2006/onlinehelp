﻿var feedback = {
    init: function () {
        feedback.registerEvents();
    },
    registerEvents: function () {
        $('.btn-active').off('click').on('click', function (e) {
            e.preventDefault();
            var btn = $(this);
            var id = btn.data('id');
            $.ajax({
                url: "/AdminManager/FeedBack/ChangeStatus",
                data: { id: id },
                dataType: "json",
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response.status == 0) {
                        btn.text('Look');
                        btn.css('background-color', 'red');

                    } else if (response.status == 1) {
                        btn.text('Open');
                        btn.css('background-color', 'green');
                    }
                }
            });
        });
    }
}
feedback.init();