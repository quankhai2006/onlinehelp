﻿using Model.Dao;
using OnlineHelp.Areas.AdminManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHelp.Areas.AdminManager.Controllers
{
    public class ChartController : Controller
    {
        // GET: AdminManager/Chart
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetJsonData()
        {
            List<RequestChart> listChart = new List<RequestChart>();
            var dao = new RequestDao();
            listChart.Add(new RequestChart("Sửa điện", dao.ListTypeRequestChart(0)));
            listChart.Add(new RequestChart("Sửa nước", dao.ListTypeRequestChart(1)));
            listChart.Add(new RequestChart("Sửa mạng", dao.ListTypeRequestChart(2)));
            listChart.Add(new RequestChart("Sửa đồ dùng", dao.ListTypeRequestChart(3)));
            return Json(listChart, JsonRequestBehavior.AllowGet);
        }
    }
}