namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Belonging")]
    public partial class Belonging
    {
        public long Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public long? RoomID { get; set; }
    }
}
