namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("General")]
    public partial class General
    {
        public long Id { get; set; }

        public long? FacilityId { get; set; }

        public long? RoomId { get; set; }

        public long? BelongingsId { get; set; }
    }
}
