﻿using Model.EF;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class FacilityDao
    {
        OnlineHelpDbContext db = null;
        public FacilityDao()
        {
            db = new OnlineHelpDbContext();
        }

        public long Insert(Facility entity)
        {
            db.Facilities.Add(entity);
            db.SaveChanges();
            return entity.Id;
        }

        public IEnumerable<Facility> ListAllPaging(string searchString, int page, int pageSize)
        {
            IQueryable<Facility> model = db.Facilities;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.Name.Contains(searchString));
            }
            return model.OrderByDescending(x => x.Id).ToPagedList(page, pageSize);
        }

        public Facility GetById(int id)
        {
            return db.Facilities.SingleOrDefault(x => x.Id == id);
        }

        public Facility ViewDetail(int id)
        {
            return db.Facilities.Find(id);
        }

        public bool Update(Facility entity)
        {
            try
            {
                var facility = db.Facilities.Find(entity.Id);
                facility.Name = entity.Name;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var facility = db.Facilities.Find(id);
                db.Facilities.Remove(facility);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public int ListCountFacility()
        {
            var listCount = db.Facilities.Count();
            return listCount;
        }
    }
}
