﻿using Model.EF;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Dao
{
    public class EmployeeDao
    {
        OnlineHelpDbContext db = null;
        public EmployeeDao()
        {
            db = new OnlineHelpDbContext();
        }

        public long Insert(Employee entity)
        {
            entity.Type = 0;
            db.Employees.Add(entity);
            db.SaveChanges();
            return entity.ID;
        }

        public IEnumerable<Employee> ListAllPaging(String searchString, int page, int pageSize)
        {
            IQueryable<Employee> model = db.Employees;
            if (!string.IsNullOrEmpty(searchString))
            {
                model = model.Where(x => x.UserName.Contains(searchString) || x.Name.Contains(searchString));
            }
            return model.OrderByDescending(x => x.CreatedDate).ToPagedList(page, pageSize);
        }

        public Employee ViewDetail(int id)
        {
            return db.Employees.Find(id);
        }

        public bool Update(Employee entity)
        {
            try
            {
                var employee = db.Employees.Find(entity.ID);
                employee.Name = entity.Name;
                if (!string.IsNullOrEmpty(entity.Password))
                {
                    employee.Password = entity.Password;
                }
                employee.Email = entity.Email;
                employee.Phone = entity.Phone;
                employee.Avartar = entity.Avartar;
                employee.Birthday = entity.Birthday;
                employee.ModifiedBy = entity.ModifiedBy;
                employee.ModifiedDate = DateTime.Now;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var employee = db.Employees.Find(id);
                db.Employees.Remove(employee);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool ChangeStatus(long id)
        {
            var employee = db.Employees.Find(id);
            employee.Status = !employee.Status;
            db.SaveChanges();
            return employee.Status;
        }

        public int ChangeType(long id)
        {
            var employee = db.Employees.Find(id);
            if (employee.Type == 0)
            {
                employee.Type = 1;
            }
            else if (employee.Type == 1)
            {
                employee.Type = 2;
            }
            else if (employee.Type == 2)
            {
                employee.Type = 3;
            }
            else if (employee.Type == 3)
            {
                employee.Type = 0;
            }
            db.SaveChanges();
            return Convert.ToInt32(employee.Type);
        }

        public Employee GetById(string userName)
        {
            return db.Employees.SingleOrDefault(x => x.UserName == userName);
        }

        public int Login(string userName, string passWord)
        {
            var result = db.Employees.SingleOrDefault(x => x.UserName == userName);
            if (result == null)
            {
                return 0;
            }
            else
            {
                if (result.Status == false)
                {
                    return -1;
                }
                else
                {
                    if (result.Password == passWord)
                    {
                        return 1;
                    }
                    else
                    {
                        return -2;
                    }
                }
            }
        }
    }
}
