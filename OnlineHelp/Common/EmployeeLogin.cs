﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineHelp.Common
{
    [Serializable]
    public class EmployeeLogin
    {
        public long EmployeeID { get; set; }
        public string UserName { get; set; }
        public string Avatar { get; set; }
        public string Name { get; set; }
        public int? Type { get; set; }
    }
}