﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class MailHelper
    {
        public bool SendMail(string toEmailAddress, string subject, string content)
        {

            string senderEmail = System.Configuration.ConfigurationManager.AppSettings["FromEmailAddress"].ToString();
            string senderPassword = System.Configuration.ConfigurationManager.AppSettings["FromEmailPassword"].ToString();

            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.EnableSsl = true;
            client.Timeout = 100000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(senderEmail, senderPassword);

            MailMessage mailMessage = new MailMessage(senderEmail, toEmailAddress, subject, content);
            mailMessage.From = new MailAddress(senderEmail, "OnlineHelp Team");
            mailMessage.IsBodyHtml = true;
            mailMessage.BodyEncoding = UTF8Encoding.UTF8;
            client.Send(mailMessage);
            return true;
        }
    }
}
