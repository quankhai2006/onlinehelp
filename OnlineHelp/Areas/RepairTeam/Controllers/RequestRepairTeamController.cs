﻿using Common;
using Model.Dao;
using Model.EF;
using OnlineHelp.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHelp.Areas.RepairTeam.Controllers
{
    public class RequestRepairTeamController : BaseRepairTeamController
    {
        OnlineHelpDbContext db = new OnlineHelpDbContext();
        // GET: RepairTeam/RequestRepairTeam
        public ActionResult Index(string searchString, int page = 1, int pageSize = 10)
        {
            var dao = new RequestRepairTeamDao();
            var typeEmloyee = (EmployeeLogin)Session[CommonConstants.EMPLOYEE_SESSION];
            var model = dao.ListAllPaging(searchString, page, pageSize, typeEmloyee.Type);
            ViewBag.SearchString = searchString;
            ViewBag.ListFacility = dao.ListFacility();
            ViewBag.ListGeneral = dao.ListGeneral();
            ViewBag.ListRoom = dao.ListRoom();
            return View(model);
        }

        [HttpPost]
        public JsonResult ChangeStatus(int id)
        {
            var result = new RequestDao().ChangeStatus(id);
            var request = db.Requests.Find(id);
            if (request.Status == 2)
            {
                string content = System.IO.File.ReadAllText(Server.MapPath("~/Assets/Client/template/finishrequest.html"));

                var toEmail = ConfigurationManager.AppSettings["ToEmailAddress"].ToString();

                new MailHelper().SendMail(request.Email, "Yêu cầu gửi từ OnlineHelp", content);
                new MailHelper().SendMail(toEmail, "Yêu cầu gửi từ OnlineHelp", content);

                return Json(new
                {
                    status = 2
                });
            }
            else if(request.Status == 1)
            {
                return Json(new
                {
                    status = 1
                });
            }
            else
            {
                return Json(new
                {
                    status = 0
                });
            }
        }

        
    }
}